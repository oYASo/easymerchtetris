#include "Square.hpp"
#include <QPainter>

Square::Square(qreal x, qreal y)
{
  setPos(x, y);
}

QRectF Square::boundingRect() const
{
  return QRectF(-5, -5, 10, 10);
}

void Square::paint(QPainter* painter,
                   const QStyleOptionGraphicsItem* option,
                   QWidget* widget)
{
  Q_UNUSED(option);
  Q_UNUSED(widget);

  painter->drawRect(-5, -5, 10, 10);
}
