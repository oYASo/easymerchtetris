#pragma once

#include <QMainWindow>

namespace Ui {
class MainWindow;
} // namespace Ui

class QGraphicsScene;
class QGraphicsItem;
class QGraphicsItemGroup;

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  explicit MainWindow(QWidget* parent = nullptr);

private:
  void update();
  void updateScore();

  void createNewElement();
  bool isElementAtScene() const;
  bool isCollision() const;
  void addToBottonGroup();
  void rotate(qreal angle);
  void removeLine();
  int step() const;

  void keyPressEvent(QKeyEvent* event) Q_DECL_OVERRIDE;

private:
  QSharedPointer<Ui::MainWindow> ui;
  QGraphicsScene* mScene;
  QGraphicsItemGroup* mCurrentElement;
  QList<QGraphicsItem*> mBottomElements;
  QTimer* const mTimer;

  int mScore;
};
