#include "MainWindow.hpp"
#include "Element.hpp"
#include "ui_MainWindow.h"
#include <QDebug>
#include <QKeyEvent>
#include <QMessageBox>
#include <QRandomGenerator>
#include <QTimer>

MainWindow::MainWindow(QWidget* parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , mScene(new QGraphicsScene(this))
    , mTimer(new QTimer(this))
    , mScore(0)
{
  ui->setupUi(this);
  mScene->setSceneRect(0, 0, 300, 600);
  ui->graphicsView->setScene(mScene);

  connect(mTimer, &QTimer::timeout, this, &MainWindow::update);
  mTimer->start(1000);

  createNewElement();

  grabKeyboard();

  sizeHint();
}

void MainWindow::update()
{
  if (!mTimer->isActive())
    return;

  mCurrentElement->moveBy(0, step());

  if (!isElementAtScene())
  {
    mCurrentElement->moveBy(0, -step());
    addToBottonGroup();
    createNewElement();
    removeLine();
    return;
  }

  if (isCollision())
  {
    if (mCurrentElement->y() < 50)
    {
      QMessageBox::information(this, "Конец", "Игра закончена");
      mTimer->stop();
    }
    mCurrentElement->moveBy(0, -step());
    addToBottonGroup();
    createNewElement();
    removeLine();
  }
}

void MainWindow::updateScore()
{
  ui->labelScore->setText(QString("Очки: %1").arg(mScore));
}

void MainWindow::createNewElement()
{
  mCurrentElement = Element::create(
      static_cast<Shape>(QRandomGenerator::global()->bounded(7) + 1));
  mScene->addItem(mCurrentElement);
  mCurrentElement->setPos(0, 0);
  mCurrentElement->setScale(3);
}

bool MainWindow::isElementAtScene() const
{
  QRectF sceneBoundingRect = mCurrentElement->sceneBoundingRect();

  if (sceneBoundingRect.left() >= 0
      && sceneBoundingRect.right() <= mScene->width()
      && sceneBoundingRect.bottom() <= mScene->height())
    return true;

  return false;
}

bool MainWindow::isCollision() const
{
  bool retVal = false;
  for (const auto& element : mBottomElements)
  {
    for (const auto& square : mCurrentElement->childItems())
    {
      auto r1 = square->sceneBoundingRect();
      auto r2 = element->sceneBoundingRect();

      retVal = !(r2.left() >= r1.right() || r2.right() <= r1.left()
                 || r2.top() >= r1.bottom() || r2.bottom() <= r1.top());

      if (retVal)
        return retVal;
    }
  }

  return retVal;
}

void MainWindow::addToBottonGroup()
{
  auto children = mCurrentElement->childItems();
  for (const auto& child : children)
  {
    mCurrentElement->removeFromGroup(child);
    mBottomElements.append(child);
  }
  mScene->removeItem(mCurrentElement);
}

void MainWindow::rotate(qreal angle)
{
  mCurrentElement->setRotation(mCurrentElement->rotation() + angle);
}

void MainWindow::removeLine()
{
  QSet<QGraphicsItem*> elementsToDelete;
  for (int y = 600; y > 0; y -= 5)
  {
    for (int x = 0; x <= 300; x += 5)
    {
      auto element = mScene->itemAt(x, y, QTransform());
      if (element && mBottomElements.contains(element))
        elementsToDelete.insert(element);
    }

    if (elementsToDelete.size() == 10)
    {
      for (const auto& etd : elementsToDelete)
      {
        mBottomElements.removeOne(etd);
        mScene->removeItem(etd);
        mScore += 10;
        updateScore();
      }

      elementsToDelete.clear();

      QSet<QGraphicsItem*> elementsToMove;
      for (int m = y; m > 0; m -= 5)
      {
        for (int n = 0; n <= 300; n += 5)
        {
          auto elementUp = mScene->itemAt(n, m, QTransform());
          if (elementUp && mBottomElements.contains(elementUp))
            elementsToMove.insert(elementUp);
        }
      }
      for (const auto& etm : elementsToMove)
        etm->moveBy(0, 30);

      y = 600;
    }

    elementsToDelete.clear();
  }
}

int MainWindow::step() const
{
  return 10 * static_cast<int>(mCurrentElement->scale());
}

void MainWindow::keyPressEvent(QKeyEvent* event)
{
  if (!mTimer->isActive())
  {
    QMainWindow::keyPressEvent(event);
    return;
  }

  switch (event->key())
  {
  case Qt::Key_Left:
    mCurrentElement->moveBy(-step(), 0);
    if (!isElementAtScene() || isCollision())
      mCurrentElement->moveBy(step(), 0);
    break;
  case Qt::Key_Right:
    mCurrentElement->moveBy(step(), 0);
    if (!isElementAtScene() || isCollision())
      mCurrentElement->moveBy(-step(), 0);
    break;
  case Qt::Key_Down:
    update();
    break;
  case Qt::Key_Up:
  {
    rotate(90.0);
    if (!isElementAtScene() || isCollision())
    {
      rotate(-90.0);
    }
    break;
  }
  default:
    QMainWindow::keyPressEvent(event);
  }
}
