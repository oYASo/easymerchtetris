#pragma once

#include <QGraphicsItem>

class Square : public QGraphicsItem
{
public:
  explicit Square(qreal x, qreal y);

private:
  QRectF boundingRect() const Q_DECL_OVERRIDE;

  void paint(QPainter* painter,
             const QStyleOptionGraphicsItem* option,
             QWidget* widget) Q_DECL_OVERRIDE;
};
