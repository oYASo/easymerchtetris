#include "Element.hpp"
#include "Square.hpp"
#include <QGraphicsRectItem>
#include <QPen>

QGraphicsItemGroup* Element::create(Shape shape)
{
  QGraphicsItemGroup* group = new QGraphicsItemGroup;

  switch (shape)
  {
  case NoShape:
    break;
  case ZShape:
  {
    group->addToGroup(new Square(5 + 10, 5));
    group->addToGroup(new Square(5 + 20, 5));
    group->addToGroup(new Square(5 + 20, 15));
    group->addToGroup(new Square(5 + 30, 15));
    break;
  }
  case SShape:
  {
    group->addToGroup(new Square(5 + 10, 15));
    group->addToGroup(new Square(5 + 20, 15));
    group->addToGroup(new Square(5 + 20, 5));
    group->addToGroup(new Square(5 + 30, 5));
    break;
  }
  case LineShape:
  {
    group->addToGroup(new Square(5 + 10, 5));
    group->addToGroup(new Square(5 + 20, 5));
    group->addToGroup(new Square(5 + 30, 5));
    group->addToGroup(new Square(5 + 40, 5));
    break;
  }
  case TShape:
  {
    group->addToGroup(new Square(5 + 10, 5));
    group->addToGroup(new Square(5 + 20, 5));
    group->addToGroup(new Square(5 + 30, 5));
    group->addToGroup(new Square(5 + 20, 15));
    break;
  }
  case SquareShape:
  {
    group->addToGroup(new Square(5 + 10, 5));
    group->addToGroup(new Square(5 + 20, 5));
    group->addToGroup(new Square(5 + 10, 15));
    group->addToGroup(new Square(5 + 20, 15));
    break;
  }
  case LShape:
  {
    group->addToGroup(new Square(5 + 10, 5));
    group->addToGroup(new Square(5 + 10, 15));
    group->addToGroup(new Square(5 + 20, 15));
    group->addToGroup(new Square(5 + 30, 15));
    break;
  }
  case MirroredLShape:
  {
    group->addToGroup(new Square(5 + 10, 15));
    group->addToGroup(new Square(5 + 20, 15));
    group->addToGroup(new Square(5 + 30, 15));
    group->addToGroup(new Square(5 + 30, 5));
    break;
  }
  }
  return group;
}
