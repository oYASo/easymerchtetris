#pragma once

#include <QGraphicsItemGroup>

enum Shape {
  NoShape,
  ZShape,
  SShape,
  LineShape,
  TShape,
  SquareShape,
  LShape,
  MirroredLShape
};

class Element
{
public:
  static QGraphicsItemGroup* create(Shape shape);
};
